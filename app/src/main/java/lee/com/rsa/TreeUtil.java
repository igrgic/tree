package lee.com.rsa;

import java.util.ArrayList;
import java.util.List;

/**
 * Tree node utility
 * author: Ivan Grgić 15.01.2019
 */
public class TreeUtil {

    public static List<Node> getFlatListFromTree(Node pRoot) {

        List<Node> tFlatList = new ArrayList<>();

        for (Node t1Node : pRoot.getChildren()) {
            t1Node.setLevel(1);
            tFlatList.add(t1Node);
            if (t1Node.isCollapsed()) {
                continue;
            }
            for (Node t2Node : t1Node.getChildren()) {
                t2Node.setLevel(2);
                tFlatList.add(t2Node);
                if (t2Node.isCollapsed()) {
                    continue;
                }
                for (Node t3Node : t2Node.getChildren()) {
                    t3Node.setLevel(3);
                    tFlatList.add(t3Node);
                    if (t3Node.isCollapsed()) {
                        continue;
                    }
                    for (Node t4Node : t3Node.getChildren()) {
                        t4Node.setLevel(4);
                        tFlatList.add(t4Node);
                        if (t4Node.isCollapsed()) {
                            continue;
                        }
                        for (Node t5Node : t4Node.getChildren()) {
                            t5Node.setLevel(5);
                            tFlatList.add(t5Node);
                            if (t5Node.isCollapsed()) {
                                continue;
                            }
                            for (Node t6Node : t5Node.getChildren()) {
                                t6Node.setLevel(6);
                                tFlatList.add(t6Node);
                                if (t6Node.isCollapsed()) {
                                    continue;
                                }
                                for (Node t7Node : t6Node.getChildren()) {
                                    t7Node.setLevel(7);
                                    tFlatList.add(t7Node);
                                    if (t7Node.isCollapsed()) {
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tFlatList;
    }

    public static Node searchTreeByRoomName(Node pRoom, String pName) {

        if (pRoom.getRoomName() == pName) {
            return pRoom;
        }

        for (Node tChild : pRoom.getChildren()) {

            Node tFound = searchTreeByRoomName(tChild, pName);

            if (tFound != null) return tFound;
        }

        return null;
    }

    public static void btnFoldClick(Node pRoot) {

        pRoot.setIsCollapsed(true);
        for (Node tChild : pRoot.getChildren()) {
            tChild.setIsCollapsed(true);
            btnFoldClick(tChild);
        }

    }

    public static void btnUnfoldClick(Node pRoom) {

        pRoom.setIsCollapsed(false);
        for (Node tChild : pRoom.getChildren()) {
            tChild.setIsCollapsed(false);
            btnUnfoldClick(tChild);
        }

    }

    public static Node searchFlatByPosition(List<Node> pFlatList, int pPosition) {
        return pFlatList.get(pPosition);
    }

    public static void unfoldSingle(Node pNode) {
        pNode.setIsCollapsed(false);
    }

    public static void foldSingle(Node pNode) {
        pNode.setIsCollapsed(true);
    }
}
