package lee.com.rsa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import tobiesoft.com.rsatree.MainActivity;
import tobiesoft.com.rsatree.R;

/**
 * Rsa tree adapter for n-tier room listing - uses FLAT lists
 * author: igrgic
 */
public class RoomAdapter extends BaseAdapter {

    private final Context iContext;
    private List<Node> iFlatList;

    public RoomAdapter(Context pContext, List<Node> pFlatList) {
        iContext = pContext;
        iFlatList = pFlatList;
    }

    public List<Node> getFlatList(){
        return iFlatList;
    };

    @Override
    public int getCount() {
        return iFlatList.size();
    }

    @Override
    public Object getItem(int pPosition) {
        return iFlatList.get(pPosition);
    }

    @Override
    public long getItemId(int pPosition) {
        return pPosition;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Node tRoom = iFlatList.get(position);

        LayoutInflater inflater = (LayoutInflater) iContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.room_list_item, parent, false);

        int tIndent = tRoom.getLevel() * 30;

        rowView.setPadding(tIndent,0,10, 0);

        TextView textView = (TextView) rowView.findViewById(R.id.firstLine);
        textView.setText(tRoom.getRoomName());
        TextView tRoomUsers = (TextView) rowView.findViewById(R.id.secondLine);
        tRoomUsers.setText(tRoom.getAllMembersAsString());

        Button tBtnAdd = (Button) rowView.findViewById(R.id.add);
        tBtnAdd.setId(position);

        ImageView tImgArrow = (ImageView) rowView.findViewById(R.id.icon);
        tImgArrow.setLabelFor(tBtnAdd.getId());

        if (tRoom.hasChildren()) {
            if (tRoom.isCollapsed()) {
                tImgArrow.setImageResource(R.drawable.forwardarrow);
            } else {
                tImgArrow.setImageResource(R.drawable.downarrow);
            }
        } else {
                tImgArrow.setVisibility(View.INVISIBLE);
        }

//        Animation animation = AnimationUtils
//                .loadAnimation(iContext, R.anim.slide_down);
//        rowView.startAnimation(animation);

        return rowView;
    }


    public void refreshItems(List<Node> pFlatList) {
        this.iFlatList = pFlatList;
        notifyDataSetChanged();
    }
}

