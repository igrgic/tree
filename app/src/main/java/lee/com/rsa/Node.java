package lee.com.rsa;

import java.util.ArrayList;
import java.util.List;

public class Node {

    private int iNodeId;
    private int iLevel;
    private String iRoomName;
    private List<String> iRoomMembers;
    private Node iParent;
    private List<Node> iChildren;
    private boolean iIsCollapsed;

    public Node(String pRoomName) {
        iRoomName = pRoomName;
        iRoomMembers = setRoomMembers();
        iChildren = new ArrayList<>();
        iIsCollapsed = true;
    }

    private List<String> setRoomMembers() {

        List<String> pMembers = new ArrayList<>();
        pMembers.add("Ante");
        pMembers.add("Emanuel");
        pMembers.add("Roko");
        return pMembers;
    }

    public void setParent(Node pParent){
        iParent = pParent;
    }

    public Node getParent(){
     return iParent;
    }

    public boolean hasChildren() {
        return iChildren.size() > 0;
    }

    public List<String> getRoomMembers(){
        return iRoomMembers;
    }

    public void addChild(Node pTreeNode) {

        pTreeNode.setParent(this);
        iChildren.add(pTreeNode);
    }

    public List<Node> getChildren() {
        return iChildren;
    }

    public void setLevel(int pLevel) {
        iLevel = pLevel;
    }

    public int getLevel() {
        return iLevel;
    }

    public boolean isCollapsed() {
        return iIsCollapsed;
    }

    public void setIsCollapsed(boolean pIsCollapsed) {
        iIsCollapsed = pIsCollapsed;
    }

    public String getRoomName() {
        return iRoomName;
    }

    public String getAllMembersAsString() {

        StringBuilder tSb = new StringBuilder();

        for (String tMember : iRoomMembers) {
            tSb.append(tMember);
            tSb.append(", ");
        }

        tSb.setLength(tSb.length() - 2);
        return tSb.toString();
    }

    public boolean isVisible() {
        return true;
    }


    public static Node searchTreeByRoomName(Node pRoom, String pName) {

        if (pRoom.getRoomName() == pName) {
            return pRoom;
        }

        for (Node tChild : pRoom.getChildren()) {

            Node tFound = searchTreeByRoomName(tChild, pName);

            if (tFound != null) return tFound;
        }

        return null;
    }
}
