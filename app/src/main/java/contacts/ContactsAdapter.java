package contacts;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import tobiesoft.com.rsatree.R;

public class ContactsAdapter extends BaseAdapter {

    Context iContext;
    List<Contacts> iContacts;

    public ContactsAdapter(Context mContext, List<Contacts> mContact) {
        this.iContext = mContext;
        this.iContacts = mContact;
    }

    @Override
    public int getCount() {
        return iContacts.size();
    }

    @Override
    public Object getItem(int position) {
        return iContacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view=View.inflate(iContext, R.layout.contacts_list_item,null);

        TextView tvName= (TextView) view.findViewById(R.id.tvContactName);
        TextView tvPhoneNumber= (TextView) view.findViewById(R.id.tvContactNumber);

        tvName.setText(iContacts.get(position).iContactName);
        tvPhoneNumber.setText(iContacts.get(position).iPhoneNumber);

        view.setTag(iContacts.get(position).iContactName);
        return view;
    }

}