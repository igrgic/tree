package tobiesoft.com.rsatree;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import java.util.List;
import lee.com.rsa.Node;
import lee.com.rsa.RoomAdapter;
import lee.com.rsa.TreeUtil;

public class MainActivity extends AppCompatActivity {

    private TextView tTextMessage;
    private ListView iTreeView;
    private Button iBtnAdd;
    private RoomAdapter iAdapter;
    private List<Node> iFlatList;
    private Node iRoot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tTextMessage = (TextView) findViewById(R.id.message);
        iTreeView = (ListView) findViewById(R.id.tree);
        iBtnAdd = (Button) findViewById(R.id.add);

        fillRooms(); // get from REST

        TreeUtil.btnFoldClick(iRoot);

        buildTree();

        initListeners();
    }

    private void buildTree() {

        iTreeView = (ListView) findViewById(R.id.tree);
        iFlatList = TreeUtil.getFlatListFromTree(iRoot);
        iAdapter = new RoomAdapter(this, iFlatList);
        iTreeView.setAdapter(iAdapter);
    }

    private void refreshTree() {

        iFlatList = TreeUtil.getFlatListFromTree(iRoot);
        iAdapter.refreshItems(iFlatList);
    }

    private void fillRooms() {

        iRoot = new Node("root");

        Node tRoom1 = new Node("Room1");
        Node tRoom2 = new Node("Room2");
        Node tRoom11 = new Node("Room11");
//        Node tRoom3 = new Node("Room3");
//        Node tRoom4 = new Node("Room4");
//        Node tRoom5 = new Node("Room5");

        Node tRoom111 = new Node("Room111");
        Node tRoom1111 = new Node("Room1111");
        Node tRoom11111 = new Node("Room11111");
        tRoom111.addChild(tRoom1111);
        tRoom1111.addChild(tRoom11111);

        tRoom11.addChild(tRoom111);

        tRoom1.addChild(tRoom11);
//        tRoom1.addChild(tRoom3);

        iRoot.addChild(tRoom1);
        iRoot.addChild(tRoom2);
//        iRoot.addChild(tRoom5);
    }

    public void arrowClick(View pView) {

        int tPosition = pView.getLabelFor();
        String pParentName = iFlatList.get(tPosition).getRoomName();
        Node tParent = TreeUtil.searchTreeByRoomName(iRoot, pParentName);

        if(tParent.isCollapsed()){
            TreeUtil.unfoldSingle(tParent);
        } else {
            TreeUtil.foldSingle(tParent);
        }

        refreshTree();
    }

    public void addRoomClick(View pView) {

        Node tChild = new Node("New room");

        String pParentName = iFlatList.get(pView.getId()).getRoomName();

        Node tParent = TreeUtil.searchTreeByRoomName(iRoot, pParentName);
        tParent.addChild(tChild);
        tParent.setIsCollapsed(false);

        refreshTree();

    }

    private void initListeners() {

        iTreeView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("RSA", "onItemClick:" + iAdapter.getFlatList().get(position).getRoomName());
            }
        });

    }

    public void btnCollapseClick(View view) {

        TreeUtil.btnFoldClick(iRoot);
        refreshTree();
    }

    public void btnExpandClick(View view) {

        TreeUtil.btnUnfoldClick(iRoot);
        refreshTree();
    }

    public void goToInviteScreen(View view) {

        Intent tIntent = new Intent(this, ContactsActivity.class);
        startActivity(tIntent);

    }
}
