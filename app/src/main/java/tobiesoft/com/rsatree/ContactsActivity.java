package tobiesoft.com.rsatree;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;

import contacts.Contacts;
import contacts.ContactsAdapter;

public class ContactsActivity extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_contacts);

        listView = (ListView) findViewById(R.id.lvContacts);

        getContacts();
        

    }

    public void getContacts() {

        ArrayList<Contacts> tAllContactsData = new ArrayList<>();

        Cursor tCursor = null;
        ContentResolver contentResolver = getContentResolver();
        try {
            tCursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        } catch (Exception ex) {
            Log.e("Error on contact", ex.getMessage());
        }

        if (tCursor.getCount() > 0) {

            while (tCursor.moveToNext()) {

                Contacts tContact = new Contacts();
                String tContactId = tCursor.getString(tCursor.getColumnIndex(ContactsContract.Contacts._ID));
                String tContactName = tCursor.getString(tCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                tContact.iContactName = tContactName;


                int hasPhoneNumber = Integer.parseInt(tCursor.getString(tCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {

                    Cursor phoneCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI
                            , null
                            , ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?"
                            , new String[]{tContactId}
                            , null);

                    while (phoneCursor.moveToNext()) {
                        String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                        tContact.iPhoneNumber = phoneNumber;

                    }
                    phoneCursor.close();
                }

                tAllContactsData.add(tContact);
            }

            ContactsAdapter adapter = new ContactsAdapter(this, tAllContactsData);
            listView.setAdapter(adapter);
        }
    }
}
